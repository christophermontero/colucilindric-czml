# ColuCilindric-czml
Esta aplicación web tiene un netamente propósito educativo, con la ayuda de dos librerías conseguimos plantar un cilindro sólido sobre un visor geográfico. El cilindro inicia su recorrido en Bogotá y pasa por:

- Mosquera
- Facatativa
- Guaduas
- Honda
- La Dorada
- Puerto Triunfo
- San Luis
- Rionegro

Para finalmente terminar su recorrido en Medellín.

------------

## Recorrido

1. Hora de inicio: 1ro de abril de 2020 a las 7:00 UTC
2. Hora de fin: 1ro de abril de 2020 a las 14:00 UTC

------------

## Librerías

- cesium: permite  crear el objeto viewer de la clase Cesium

```javascript
let viewer = new Cesium.Viewer("cesiumViewer");
```

- czml: permite agregar un documento czml dentro del objeto viewer

```javascript
let czml = new Cesium.CzmlDataSource();
let element = czml.load("stage.czml");
viewer.dataSources.add(czml);
```

